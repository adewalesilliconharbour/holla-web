(function () {



   var owlCarousel = function () {

      new WOW().init();

      $('.owl-carousel').owlCarousel({
         smartSpeed: 2000,
         nav: false,
         loop: false,
         center: false,
         dots: false,
         items: 1,
         autoWidth: true,
         merge: true,
         lazyload: false,
         navText: [
            "<i class='fa carousel-left-arrow fa-chevron-left'></i>",
            "<i class='fa carousel-right-arrow fa-chevron-right'></i>"
         ], responsiveClass: true,
         responsive: {
            0: {
               items: 1,
               // nav: true,
               autoplay: true,
               loop: true
            },
            768: {
               items: 1,
               // nav: true,
               loop: true,
               autoplay: true,
               // autoplayTimeout: 1500,
               navText: [
                  "<i class='fa carousel-left-arrow fa-chevron-left'></i>",
                  "<i class='fa carousel-right-arrow fa-chevron-right'></i>"
               ],
            }
         }
      });

   };

   $.fn.goTo = function () {
      $('html, body').animate({
         scrollTop: $(this).offset().top + 'px'
      }, 'slow');
      return this; // for chaining...
   }

   $(function () {
      owlCarousel();
   });


}());