import React from 'react';
import './css/style.css';
import HomePage from './pages/home'
import ContactUS from './pages/contact-us'
import { Switch, Route, BrowserRouter } from 'react-router-dom'


export default () => (
   <BrowserRouter>
      <Switch>
         <Route exact path="/" component={HomePage} />
         <Route exact path="/contact-us" component={ContactUS} />
      </Switch>
   </BrowserRouter>
)
