import React from 'react';
import { Header, Footer } from './component'
import PhoneImg from '../img/phone-holla.png'
import AppstoreLogo from '../img/appstore.jpg'
import PlayStore from '../img/googleplay.jpg'

const Home = () => {
   // showing the modal form
   const showModal = () => {
      document.querySelector('#modal').classList.remove('d-none')
   }
   return (
      <div id="page-wrap">
         <div id="fh5co-hero-wrapper">
            <Header />
         </div>
         <div className="fh5co-advantages-outer">
            <div className="container">
               <div className="row center-text">
                  <div className="col-sm-8 col-md-8">
                     <h1 className="text-white earn-money font-bold">Earn More Money</h1>
                     <p style={{ fontSize: '23px' }} className="text-white">Get more clients</p>
                  </div>
                  <div className="col-sm-4 col-md-4 right-align">
                     <button className="btn btn-register" onClick={showModal}>Sign Up</button>
                  </div>
               </div>
            </div>
         </div>
         <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
            <ol className="carousel-indicators">
               <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
               <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
               <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div className="carousel-inner">
               <div className="carousel-item active">
                  <div className="row">
                     <div className="col-md-6 in-order-5">
                        <div className="col-sm-image-container">
                           <div className="circular">
                              <img className="img-float-left" src={PhoneImg} alt="smartphone-3" />
                           </div>
                        </div>
                     </div>
                     <div className="col-md-6 in-order-6 sm-6-content">
                        <div className="single-feature">
                           <h3>Client</h3>
                           <ul>
                              <li>Get any service you need as quickly as possible</li>
                              <li>Quality services at affordable prices</li>
                              <li>Safe and convenient service</li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
               {/* Second Slide */}
               <div className="carousel-item">
                  <div className="row">
                     <div className="col-md-6 in-order-5">
                        <div className="col-sm-image-container">
                           <div className="circular">
                              <img className="img-float-left" src={PhoneImg} alt="smartphone-3" />
                           </div>
                        </div>
                     </div>
                     <div className="col-md-6 in-order-6 sm-6-content">
                        <div className="single-feature">
                           <h3>Worker</h3>
                           <ul>
                              <li>Make more money while spending less</li>
                              <li>Enjoy a better work experience</li>
                              <li>Get more clients without a hassle</li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
               {/* Third slide */}
               <div className="carousel-item">
                  <div className="row">
                     <div className="col-md-6 in-order-5">
                        <div className="col-sm-image-container">
                           <div className="circular">
                              <img className="img-float-left" src={PhoneImg} alt="smartphone-3" />
                           </div>
                        </div>
                     </div>
                     <div className="col-md-6 in-order-6 sm-6-content">
                        <div className="single-feature">
                           <h3>Hub</h3>
                           <ul>
                              <li>Get connected to Professional consultants</li>
                              <li>Be empowered with resources and tools</li>
                              <li>Join a community of workers, contractors and much more</li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>

         <div className="service-minute" style={{ padding: '90px 70px', background: '#fff', marginTop: '1rem' }}>
            <div className="container">
               <div className="row center-text">
                  <div className="col-md-8">
                     <h1 className="mt-3 earn-money font-bold">Get Services In A Minute</h1>
                  </div>
                  <div className="col-md-4 right-align">
                     <button className="btn btn-register _color" onClick={showModal}>Holla @ Us</button>
                  </div>
               </div>
            </div>
         </div>

         <section style={{ padding: '1rem 0px' }}>
            <div className="container">
               <div className="row" style={{ alignItems: 'center' }}>
                  <div className="col-md-6">
                     <img src={PhoneImg} alt="Phone" style={{ width: '100%', maxHeight: '' }} />
                  </div>
                  <div className="col-md-6 right-align" style={{ overflow: 'hidden' }}>
                     <div className="">
                        <a className="twitter-timeline" data-height="350" data-width="500" data-theme="light" href="https://twitter.com/hollajobs?ref_src=twsrc%5Etfw">Tweets by hollajobs</a>
                     </div>
                     <div className="mt-2">
                        <div className="fb-page" data-href="https://www.facebook.com/Hollajobs" data-tabs="timeline" data-width="500" data-height="350" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="true">
                           <blockquote cite="https://www.facebook.com/Hollajobs" className="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Hollajobs">Hollajobs</a></blockquote>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>

         {/* Download section */}
         <div id="fh5co-download" className="fh5co-bottom-outer" style={{ padding: '90px 70px' }}>
            {/* <div className="overlay"> */}
            <div className="container fh5co-bottom-inner">
               <div className="row center-text">
                  <div className="col-sm-12 center">
                     <h1 className="text-white text-center">App Available Soon On</h1>
                  </div>
                  <div className="col-sm-6 appstore-logo">
                     {/* <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque suscipit, similique animi saepe, ipsam itaque, tempore minus maxime pariatur error unde fugit tenetur.</p> */}
                     <a className="wow fadeIn animated" data-wow-delay="0.25s" href="#">
                        <img className="" src={AppstoreLogo} alt="App Store Icon" /></a>
                  </div>
                  <div className="col-sm-6">
                     <a className="wow fadeIn animated" data-wow-delay="0.67s" href="#">
                        <img className="" src={PlayStore} alt="Google Play Icon" /></a>
                  </div>
                  {/* </div> */}
               </div>
            </div>
         </div>
         {/* Footer Section */}
         <Footer />
      </div >
   )
}
export default Home;