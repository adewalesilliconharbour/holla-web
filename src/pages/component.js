import React, { Fragment } from 'react';
import Logo from '../img/logo.png'
import PhoneImg from '../img/phone-holla.png'
import ImageHollaMap from '../img/Splash-1.png'
import ImageHolla from '../img/Splash-2.png'
// import PhoneImgg from '../img/hollascreen.png'
import Hart from '../img/pin.png';
import axios from 'axios';
// import PhoneImgg from '../img/hollascreen.png'
// import Hart from '../img/pin.png'

export const Header = () => {
   // showing the survey form
   const showSurvey = () => {
      document.querySelector('.survey-section').classList.toggle('d-none')
   }
   // showing the modal form
   const showModal = () => {
      document.querySelector('#modal').classList.remove('d-none')
   }


   // Close the modal form
   const closeModal = (e) => {
      if (e.target.id === 'modal') {
         document.querySelector('#modal').classList.add('d-none')
      }
   }
   const Registration = (e) => {
      e.preventDefault()
      let { fname, lname, email, phone } = e.target.elements
      if (fname.value === "" || lname.value === "" || email.value === "" || phone.value === "") {
         return document.querySelector('.error').textContent = 'Some fields are missing';
      }
      fname = fname.value.trim()
      lname = lname.value.trim()
      email = email.value.trim()
      phone = phone.value.trim()

      if (!/^[a-z\-]+$/i.test(fname)) {
         return document.querySelector('.error').textContent = 'Use only alphabets for first name';
      }
      if (!/^[a-z\-]+$/i.test(lname)) {
         return document.querySelector('.error').textContent = 'Use only alphabets for last name';
      }

      // check phone
      if (phone.length !== 11) {
         return document.querySelector('.error').textContent = '11 digits required for phone number';
      }
      if (isNaN(phone)) {
         return document.querySelector('.error').textContent = 'Invalid phone number';
      }
      document.querySelector('#submit_btn').textContent = 'Loading...';
      let url = 'https://hollatest.herokuapp.com/api/v1/auth/preregistration';

      let dataSubmit = {
         firstName: fname,
         lastName: lname,
         email: email,
         phone: phone
      }
      // submiting the data
      axios.post(url, dataSubmit, {
         'content-type': 'application/json'
      }).then(res => {
         if (res.status === 200) {
            if (res.data.error) {
               document.querySelector('#submit_btn').textContent = 'Register';
               document.querySelector('.error').textContent = res.data.message;
            } else {
               document.querySelector('.modal-container').innerHTML = `<div className="text-center">
               <h3 className="text-green text-center">Registration Successful!</h3>
               <p className="mt-3 text-center">Thank you for your interest in Holla.</p>
            </div>`
            }
         } else {
            document.querySelector('.error').textContent = 'Something went wrong please try again later';
         }
      })
   }
   return (
      <Fragment>
         {/* Modal Section */}
         <div className="modal d-none" id="modal" onClick={closeModal}>
            <div className="modal-container text-center">
               <h6 className="text-center mb-3 py-3">Thank you for your interest in Holla. Please fill in the form below.</h6>
               <form name="reg-form" onSubmit={Registration}>
                  <div className="error"></div>
                  <input type="text" name="fname" className="form-control" placeholder="First Name" autoComplete="given-name" />
                  <input type="text" name="lname" className="form-control" placeholder="Last Name" autoComplete="family-name" />
                  <input type="email" name="email" className="form-control" placeholder="Email" autoComplete="email" />
                  <input type="tel" name="phone" className="form-control" placeholder="Phone" autoComplete="phone" />
                  <button type="submit" id="submit_btn" className="btn text-white" style={{ width: '100%', padding: '11px 15px', background: '#3885A2' }}>Register</button>
               </form>
               {/* <div className="text-center">
                  <h3 className="text-green text-center">Registration Successful!</h3>
                  <p className="mt-3 text-center">Thank you for your interest in Holla.</p>
               </div> */}
            </div>
         </div>
         {/* Header Navigation section */}
         <nav className="container navbar navbar-expand-lg main-navbar-nav navbar-light">
            <a className="navbar-brand" href="index.html">
               <img src={Logo} alt="Logo" style={{ width: '150px' }} />
            </a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
               <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
               <ul className="navbar-nav nav-items-center ml-auto">
                  <li className="nav-item active">
                     <a className="nav-link" href="/">Home <span className="sr-only">(current)</span></a>
                  </li>
                  {/* <li className="nav-item">
               <a className="nav-link" href="index.html#">Features</a>
            </li> */}
                  <li className="nav-item">
                     <a className="nav-link" href="/contact-us">Contact Us</a>
                  </li>
                  <li className="nav-item">
                     <a className="nav-link" href="#fh5co-download">Download App</a>
                  </li>
               </ul>
               {/* <div className="social-icons-header">
                  <a href="https://www.facebook.com/Hollajobs/"><i className="fab fa-facebook-f"></i></a>
                  <a href="https://www.instagram.com/hollajobs/"><i className="fab fa-instagram"></i></a>
                  <a href="https://www.twitter.com/hollajobs"><i className="fab fa-twitter"></i></a>
               </div> */}
            </div>
         </nav>
         {/* Header banner section */}
         <div className="container fh5co-hero-inner">
            <div className="row center-text">
               <div className="col-md-8 text-white">
                  <h1 className="header-caption">Jobs on demand<br /><span style={{ textTransform: 'lowercase' }} > ...near u</span>
                     <img src={Hart} className="hart-img" alt="Hart" />
                  </h1>
                  <div className="text-white features_oppt">
                     <div className="header-desc">
                        <p>All the services you need at the palm of your hand</p>
                     </div>
                  </div>
               </div>
               {/* Screen Slider section */}
               <div className="col-md-4 header-img" data-center="false" data-autowidth="false">
                  <div>
                     <img src={PhoneImg} alt="Phone" className="img" />
                  </div>
                  <div className="caro-img">
                     <div className="owl-carousel owl-theme">
                        <div className="item"><img src={ImageHolla} alt="" /></div>
                        <div className="item"><img src={ImageHollaMap} alt="" /></div>
                     </div>
                  </div>
               </div>
               <div className="col-md-6">
                  <div className="CTA_btn">
                     <button className="btn btn-md download-btn-first wow fadeInLeft animated" data-wow-delay="0.85s" onClick={showSurvey}>Take Our Survey</button>
                     <button className="btn btn-md features-btn-first animated fadeInLeft wow" data-wow-delay="0.95s" onClick={showModal}>Pre-Register Here</button>
                     <ul className="survey-section d-none">
                        <li>
                           <a href="https://forms.gle/MCVYub1HjLFTVyTM9">Client Survey</a>
                        </li>
                        <li>
                           <a href="https://forms.gle/yN81aey78nxCGK9BA">Worker Survey</a>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </Fragment>
   )
}

export const Footer = () => (
   <footer className="footer-outer">
      <div className="container footer-inner">
         <div className="row wow fadeIn animated" data-wow-delay="0.66s">
            {/* <div className="footer-three-grid wow fadeIn animated" data-wow-delay="0.66s"> */}
            <div className="col-md-3">
               <img src={Logo} alt="Logo" style={{ width: '150px' }} />
            </div>
            <div className="col-md-6">
               <nav className="footer-nav">
                  <ul>
                     <a href="/"><li className="active">Home</li></a>
                     {/* <a href="index.html#"><li>Features</li></a> */}
                     <a href="/contact-us"><li>Contact Us</li></a>
                     <a href="#fh5co-download"><li>Download App</li></a>
                  </ul>
               </nav>
            </div>
            <div className="col-md-3 right-align">
               <div className="social-icons-footer">
                  <a href="https://www.facebook.com/Hollajobs/"><i className="fab fa-facebook-f"></i></a>
                  <a href="https://www.instagram.com/hollajobs/"><i className="fab fa-instagram"></i></a>
                  <a href="https://www.twitter.com/hollajobs"><i className="fab fa-twitter"></i></a>
               </div>
               <div style={{ color: '#eee', fontSize: '13px' }}>
                  <div>Info@hollajobs.com</div>
                  <div className="mt-1">+2349068000095</div>
               </div>
            </div>
         </div>

         <span className="border-bottom-footer"></span>

         <p className="copyright">&copy; 2019  All rights reserved.</p>

      </div>
   </footer>
)