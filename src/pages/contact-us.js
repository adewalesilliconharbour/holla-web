import React from 'react';
import { Header, Footer } from './component'
export default () => {
   return (
      <div id="page-wrap">
         <div id="fh5co-hero-wrapper">
            <Header />
         </div>
         <section style={{ background: '#eee' }}>
            <div className="container">
               <div className="contact-form">
                  <h5>Send us a message</h5>
                  <form className="mt-3">
                     <input className="form-control" placeholder="Name" autoComplete="given-name" />
                     <input className="form-control" placeholder="Email" autoComplete="email" />
                     <input className="form-control" placeholder="Phone" autoComplete="phone" />
                     <textarea rows="7" className="form-control" placeholder="Message"></textarea>
                     <button type="submit" className="btn mt-3 text-white" style={{ width: '100%', padding: '11px 15px', background: '#3885A2' }}>Submit</button>
                  </form>
               </div>
            </div>
         </section>
         {/* Footer Section */}
         <Footer />
      </div >
   )
}